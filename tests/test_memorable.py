import memorable


def test_can_generate_name():
    """Should be able to generate a name."""
    memorable.name()


def test_can_generate_action():
    """Should be able to generate a action."""
    memorable.action()


def test_can_generate_thing():
    """Should be able to generate a thing."""
    memorable.thing()


def test_can_generate_thing_with_specific_kind():
    """Should be able to generate a thing with a specific kind."""
    memorable.thing(kind=memorable.NounTypes.HOUSEHOLDS, extra_characters=4)


def test_can_generate_code_phrase():
    """Should be able to generate a code phrase."""
    memorable.code_phrase()
