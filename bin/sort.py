#!/usr/bin/env python3
"""
Script for keeping resource files tidy. It sorts the contents of each resource
file removing any duplicates within the file.
"""
import os

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))
RESOURCES_DIR = os.path.join(PROJECT_DIR, "memorable", "_resources")

resources = []
for directory, _, files in os.walk(RESOURCES_DIR):
    for file in files:
        resources.append(os.path.join(directory, file))

for resource in resources:
    with open(resource) as f:
        lines = [l.lower() for l in f.read().strip().split("\n")]
    lines = sorted(list(set(lines)))
    with open(resource, "w") as f:
        f.write("\n".join(lines) + "\n")
